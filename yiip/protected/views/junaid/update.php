<?php
/* @var $this JunaidController */
/* @var $model Junaid */

$this->breadcrumbs=array(
	'Junaids'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Junaid', 'url'=>array('index')),
	array('label'=>'Create Junaid', 'url'=>array('create')),
	array('label'=>'View Junaid', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Junaid', 'url'=>array('admin')),
);
?>

<h1>Update Junaid <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>