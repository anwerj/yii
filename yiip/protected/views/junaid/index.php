<?php
/* @var $this JunaidController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Junaids',
);

$this->menu=array(
	array('label'=>'Create Junaid', 'url'=>array('create')),
	array('label'=>'Manage Junaid', 'url'=>array('admin')),
);
?>

<h1>Junaids</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
