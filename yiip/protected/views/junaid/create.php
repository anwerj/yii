<?php
/* @var $this JunaidController */
/* @var $model Junaid */

$this->breadcrumbs=array(
	'Junaids'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Junaid', 'url'=>array('index')),
	array('label'=>'Manage Junaid', 'url'=>array('admin')),
);
?>

<h1>Create Junaid</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>