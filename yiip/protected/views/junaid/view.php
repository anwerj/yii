<?php
/* @var $this JunaidController */
/* @var $model Junaid */

$this->breadcrumbs=array(
	'Junaids'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Junaid', 'url'=>array('index')),
	array('label'=>'Create Junaid', 'url'=>array('create')),
	array('label'=>'Update Junaid', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Junaid', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Junaid', 'url'=>array('admin')),
);
?>

<h1>View Junaid #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fname',
		'lname',
	),
)); ?>
