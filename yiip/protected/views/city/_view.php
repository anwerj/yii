<?php
/* @var $this CityController */
/* @var $data City */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CountryCode')); ?>:</b>
	<?php echo CHtml::encode($data->CountryCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('District')); ?>:</b>
	<?php echo CHtml::encode($data->District); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Population')); ?>:</b>
	<?php echo CHtml::encode($data->Population); ?>
	<br />


</div>