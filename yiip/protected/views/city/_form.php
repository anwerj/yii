<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'city-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Name'); ?>
		<?php echo $form->textField($model,'Name',array('size'=>35,'maxlength'=>35)); ?>
		<?php echo $form->error($model,'Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CountryCode'); ?>
		<?php echo $form->textField($model,'CountryCode',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'CountryCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'District'); ?>
		<?php echo $form->textField($model,'District',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'District'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Population'); ?>
		<?php echo $form->textField($model,'Population'); ?>
		<?php echo $form->error($model,'Population'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->